Jobs Test Application
========================

This test was developed and tested using the technologies below:

  * PHP - 5.6
	
  * Symfony - 3.2
	
  * Mysql - 5.5

How to deploy this application
--------------

Below a step-by-step guide to make things work

  * Clone this repository

  * Run: "composer install" 

  * Run: "php bin/console doctrine:migration:migrate"

  * Run: "php bin/console assetic:dump -env=prod --no-debug" (This step is needed only in a production environment)

  * Run: "php bin/console doctrine:fixtures:load" (Run this one only if you want some fake data to test)
  
Third party code used
--------------

Aditional Bundles used with Symfony:

  * [**AsseticBundle**][1] - Integrate Assetic an asset management 
  
  * [**FriendsOfSymfony - UserBundle**][2] - Adds support for user management.

  * [**Doctrine Database Migrations**][3] - Adds versioning for database schema and easily deploy changes.

  * [**Faker**][4] - Library for generates fake data.

Frontend:

  * [**Bootstrap**][5] - HTML, CSS and JS framework.
  
  * [**SBAdmin 2**][6] - Admin template for bootstrap framework.
  
  * [**FontAwesome**][7] - Customizable vector icons

Demo version
--------------

You can see a demo version of this application clicking [here][8]

[1]:  https://github.com/symfony/assetic-bundle
[2]:  https://github.com/FriendsOfSymfony/FOSUserBundle
[3]:  https://github.com/doctrine/migrations
[4]:  https://github.com/fzaninotto/Faker
[5]:  http://getbootstrap.com/
[6]:  http://getbootstrap.com/
[7]:  http://fontawesome.io/
[8]:  http://jobstest.rntech.com.br