<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;

class LoadUserData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
	
	/**
	 * @var ContainerInterface
	 */
	private $container;
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
	}
	
	public function load(ObjectManager $manager)
	{
		$userManager = $this->container->get('fos_user.user_manager');

        $userAdmin = $userManager->createUser();
        $userAdmin->setUsername('admin');
        $userAdmin->setEmail('admin@domain.com');
        $userAdmin->setPlainPassword('ad123min');
        $userAdmin->setEnabled(true);
        $userAdmin->setRoles(array('ROLE_ADMIN'));
        
        $userManager->updateUser($userAdmin, true);
        $this->addReference('admin-user', $userAdmin);
        
        $userEmployer = $userManager->createUser();
        $userEmployer->setUsername('employer');
        $userEmployer->setEmail('employer@domain.com');
        $userEmployer->setPlainPassword('empl123oyer');
        $userEmployer->setEnabled(true);
        $userEmployer->setRoles(array('ROLE_VISITOR'));

        $userManager->updateUser($userEmployer, true);
        $this->addReference('employer-user', $userEmployer);
        
        $userEmployer2 = $userManager->createUser();
        $userEmployer2->setUsername('employer2');
        $userEmployer2->setEmail('employer2@domain.com');
        $userEmployer2->setPlainPassword('empl123oyer');
        $userEmployer2->setEnabled(true);
        $userEmployer2->setRoles(array('ROLE_VISITOR'));
        
        $userManager->updateUser($userEmployer2, true);
        $this->addReference('employer2-user', $userEmployer2);
	}
	

	public function getOrder()
	{
		return 1;
	}
}