<?php

namespace AdminBundle\Controller;

use EmployerBundle\Entity\Vacancy;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Vacancy controller.
 *
 * @Route("/vacancy")
 */
class VacancyController extends Controller
{
	private $filters = array(
		'all' => 'All',
		'today' => 'Today', 
		'week' => 'This week', 
		'month' => 'This month'
	);
	
    /**
     * Lists all vacancy entities.
     *
     * @Route(
     * 		"/{filter}", 
     * 		name="admin_vacancy_index",
     * 		defaults={
     * 			"filter": "all"
     * 		},
     * 		requirements={
     * 			"filter": "all|today|week|month"
     * 		}
     * )
     * @Method("GET")
     */
    public function indexAction($filter)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $vacancies = $em->getRepository('EmployerBundle:Vacancy')->getFiltered($filter);

        return $this->render('AdminBundle:Vacancy:index.html.twig', array(
            'vacancies' => $vacancies,
        	'filters' => $this->filters,
        	'filter_selected' => $filter
        ));
    }

    /**
     * Finds and displays a vacancy entity.
     *
     * @Route("/{id}", name="admin_vacancy_show")
     * @Method("GET")
     */
    public function showAction(Vacancy $vacancy)
    {
        $deleteForm = $this->createDeleteForm($vacancy);
		
        $this->denyAccessUnlessGranted('view', $vacancy);
        
        return $this->render('AdminBundle:Vacancy:show.html.twig', array(
            'vacancy' => $vacancy,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing vacancy entity.
     *
     * @Route("/{id}/edit", name="admin_vacancy_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Vacancy $vacancy)
    {
        $deleteForm = $this->createDeleteForm($vacancy);
        $editForm = $this->createForm('EmployerBundle\Form\VacancyType', $vacancy);
        $editForm->handleRequest($request);
		
        $this->denyAccessUnlessGranted('edit', $vacancy);
        
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_vacancy_edit', array('id' => $vacancy->getId()));
        }

        return $this->render('AdminBundle:Vacancy:edit.html.twig', array(
            'vacancy' => $vacancy,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a vacancy entity.
     *
     * @Route("/{id}", name="admin_vacancy_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Vacancy $vacancy)
    {
        $form = $this->createDeleteForm($vacancy);
        $form->handleRequest($request);

        $this->denyAccessUnlessGranted('edit', $vacancy);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($vacancy);
            $em->flush($vacancy);
        }

        return $this->redirectToRoute('admin_vacancy_index');
    }

    /**
     * Creates a form to delete a vacancy entity.
     *
     * @param Vacancy $vacancy The vacancy entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Vacancy $vacancy)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_vacancy_delete', array('id' => $vacancy->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
