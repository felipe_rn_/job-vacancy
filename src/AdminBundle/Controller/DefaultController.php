<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="admin_dashboard")
     */
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$total_vacancies = $em->getRepository('EmployerBundle:Vacancy')->countToday();
    	$total_users = $em->getRepository('AdminBundle:User')->count();
    	
        return $this->render('AdminBundle:Default:index.html.twig', array(
        		'total_vacancies' => $total_vacancies,
        		'total_users' => $total_users
        ));
    }
    
}
