<?php

namespace SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use EmployerBundle\Entity\Vacancy;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$vacancies = $em->getRepository('EmployerBundle:Vacancy')->findByEnabled(true);
    
        return $this->render('SiteBundle:Default:index.html.twig', array(
        	'vacancies' => $vacancies
        ));
    }
    
}
