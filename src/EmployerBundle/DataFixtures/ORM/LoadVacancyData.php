<?php

namespace EmployerBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use EmployerBundle\Entity\Vacancy;
use Faker\Factory;

class LoadVacancyData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
	
	/**
	 * @var ContainerInterface
	 */
	private $container;
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
	}
	
	public function load(ObjectManager $manager)
	{
		$faker = Factory::create();
		
		for($i = 1; $i <= 5; $i ++){
			$vacancy = new Vacancy();
			$vacancy->setTitle($faker->company);
			$vacancy->setDescription($faker->text);
			$vacancy->setEnabled($faker->boolean);
			$vacancy->setCreatedAt($faker->dateTimeThisMonth);
			$vacancy->setOwner($this->getReference('employer-user'));
			$manager->persist($vacancy);
		}
			
		for($i = 1; $i <= 5; $i ++){
			$vacancy = new Vacancy();
			$vacancy->setTitle($faker->company);
			$vacancy->setDescription($faker->text);
			$vacancy->setEnabled($faker->boolean);
			$vacancy->setCreatedAt($faker->date(''));
			$vacancy->setOwner($this->getReference('employer2-user'));
			$manager->persist($vacancy);
		}
		
		$manager->flush();
	        
	}
	
	public function getOrder()
	{
		// the order in which fixtures will be loaded
		// the lower the number, the sooner that this fixture is loaded
		return 2;
	}
}