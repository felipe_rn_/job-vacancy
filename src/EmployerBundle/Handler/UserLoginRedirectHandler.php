<?php 
namespace EmployerBundle\Handler;

use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class UserLoginRedirectHandler implements AuthenticationSuccessHandlerInterface
{
  protected $router;
  protected $security;

  public function __construct(Router $router, AuthorizationChecker $security)
  {
    $this->router = $router;
    $this->security = $security;
  }

  public function onAuthenticationSuccess(Request $request, TokenInterface $token)
  {
    if ($this->security->isGranted('ROLE_ADMIN'))
    {
      $response = new RedirectResponse($this->router->generate('admin_dashboard'));
    } else
    {
      $referer_url = $this->router->generate('employer_dashboard');
      $response = new RedirectResponse($referer_url);
    }
    return $response;
  }
}