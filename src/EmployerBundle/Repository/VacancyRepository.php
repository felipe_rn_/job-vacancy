<?php

namespace EmployerBundle\Repository;

use AdminBundle;
use AdminBundle\Entity\User;

/**
 * VacancyRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class VacancyRepository extends \Doctrine\ORM\EntityRepository
{
	public function getFiltered($filter){
		
		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder();
		$qb->select('v')
			->from('EmployerBundle:Vacancy', 'v');
		
		switch ($filter){
			case 'today':
				$qb->where('v.createdAt >= :created_at')
					->setParameter('created_at', date("Y-m-d", time()));
				break;
			case 'week':
				
				$start_week = date("Y-m-d",strtotime('monday this week'));
				$end_week = date("Y-m-d",strtotime('sunday this week'));
				
				$qb->where('v.createdAt >= :start')
			        ->andWhere('v.createdAt <= :end')
			        ->setParameter('start',$start_week)                      
			        ->setParameter('end',$end_week);
				break;
			case 'month':
			
				$day = date("Y-m-d", strtotime('first day of this month'));
			
				$qb->where('v.createdAt >= :day')
				->setParameter('day', $day);
				break;
		}
		
		return $qb->getQuery()->getResult();
	}
	
	public function countToday(){
		$em = $this->getEntityManager();
		$query = $em->createQuery('SELECT COUNT(v.id) FROM EmployerBundle:Vacancy v WHERE v.createdAt >= :created_at')
			->setParameter('created_at', date("Y-m-d", time()));
		$count = $query->getSingleScalarResult();
		return $count;
	}
	
	public function count(User $user){
		$em = $this->getEntityManager();
		$query = $em->createQuery('SELECT COUNT(v.id) FROM EmployerBundle:Vacancy v WHERE v.owner = :owner')
			->setParameter('owner', $user);
		$count = $query->getSingleScalarResult();
		return $count;
	}
}
