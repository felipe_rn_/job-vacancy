<?php 
namespace EmployerBundle\Security;

use EmployerBundle\Entity\Vacancy;
use AdminBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

class VacancyVoter extends Voter
{
    // these strings are just invented: you can use anything
    const VIEW = 'view';
    const EDIT = 'edit';
    
    private $decisionManager;
    
    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
    	$this->decisionManager = $decisionManager;
    	
    }
        
    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VIEW, self::EDIT))) {
            return false;
        }
        
        // only vote on Vacancy objects inside this voter
        if (!$subject instanceof Vacancy) {
            return false;
        }
        
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            
            return false;
        }

        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
        	return true;
        }
        
        // you know $subject is a Vacancy object, thanks to supports
        /** @var Vacancy $vacancy */
        $vacancy = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($vacancy, $user);
            case self::EDIT:
                return $this->canEdit($vacancy, $user);            
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(Vacancy $vacancy, User $user)
    {
        // if they can edit, they can view
        if ($this->canEdit($vacancy, $user)) {
            return true;
        }
        
    }

    private function canEdit(Vacancy $vacancy, User $user)
    {
        // this assumes that the data object has a getOwner() method
        // to get the entity of the user who owns this data object
        return $user === $vacancy->getOwner();
    }
}