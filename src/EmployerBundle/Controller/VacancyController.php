<?php

namespace EmployerBundle\Controller;

use EmployerBundle\Entity\Vacancy;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Vacancy controller.
 *
 * @Route("vacancy")
 */
class VacancyController extends Controller
{
    /**
     * Lists all vacancy entities.
     *
     * @Route("/", name="vacancy_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $vacancies = $em->getRepository('EmployerBundle:Vacancy')->findByOwner($user->getId());

        return $this->render('EmployerBundle:Vacancy:index.html.twig', array(
            'vacancies' => $vacancies,
        ));
    }

    /**
     * Creates a new vacancy entity.
     *
     * @Route("/new", name="vacancy_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $vacancy = new Vacancy();
        $form = $this->createForm('EmployerBundle\Form\VacancyType', $vacancy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
        	$user = $this->get('security.token_storage')->getToken()->getUser();
        	$vacancy->setOwner($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($vacancy);
            $em->flush($vacancy);

            return $this->redirectToRoute('vacancy_show', array('id' => $vacancy->getId()));
        }

        return $this->render('EmployerBundle:Vacancy:new.html.twig', array(
            'vacancy' => $vacancy,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a vacancy entity.
     *
     * @Route("/{id}", name="vacancy_show")
     * @Method("GET")
     */
    public function showAction(Vacancy $vacancy)
    {
        $deleteForm = $this->createDeleteForm($vacancy);
		
        $this->denyAccessUnlessGranted('view', $vacancy);
        
        return $this->render('EmployerBundle:Vacancy:show.html.twig', array(
            'vacancy' => $vacancy,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing vacancy entity.
     *
     * @Route("/{id}/edit", name="vacancy_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Vacancy $vacancy)
    {
        $deleteForm = $this->createDeleteForm($vacancy);
        $editForm = $this->createForm('EmployerBundle\Form\VacancyType', $vacancy);
        $editForm->handleRequest($request);
		
        $this->denyAccessUnlessGranted('edit', $vacancy);
        
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('vacancy_edit', array('id' => $vacancy->getId()));
        }

        return $this->render('EmployerBundle:Vacancy:edit.html.twig', array(
            'vacancy' => $vacancy,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a vacancy entity.
     *
     * @Route("/{id}", name="vacancy_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Vacancy $vacancy)
    {
        $form = $this->createDeleteForm($vacancy);
        $form->handleRequest($request);

        $this->denyAccessUnlessGranted('edit', $vacancy);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($vacancy);
            $em->flush($vacancy);
        }

        return $this->redirectToRoute('vacancy_index');
    }

    /**
     * Creates a form to delete a vacancy entity.
     *
     * @param Vacancy $vacancy The vacancy entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Vacancy $vacancy)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('vacancy_delete', array('id' => $vacancy->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
