<?php

namespace EmployerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="employer_dashboard")
     */
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$user = $this->get('security.token_storage')->getToken()->getUser();
    	$total_vacancies = $em->getRepository('EmployerBundle:Vacancy')->count($user);
    	
    	return $this->render('EmployerBundle:Default:index.html.twig', array(
    			'total_vacancies' => $total_vacancies,
    	));

    }
}
